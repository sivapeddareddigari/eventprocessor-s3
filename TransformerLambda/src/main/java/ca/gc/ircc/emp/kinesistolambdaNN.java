package ca.gc.ircc.emp;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.KinesisEvent;
import com.amazonaws.util.Base64;

import java.io.*;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

public class kinesistolambdaNN implements RequestHandler<KinesisEvent, Void> {


    private final HttpClient httpClient = HttpClient.newBuilder().build();

    @Override
    public Void handleRequest(KinesisEvent KinesisEvent, Context context) {
        LambdaLogger logger = context.getLogger();
        Map<String, String> data = new LinkedHashMap<>();
        URL url = null;
        try {
            url = new URL("http://a0ce03a4068db4dd6ab603ccd3a91a00-3926295aa56ff929.elb.ca-central-1.amazonaws.com:443/app.log");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection http = null;
        try {
            http = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            http.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        http.setDoOutput(true);
        http.setRequestProperty("Accept", "application/json");
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("User-Agent", "Kinesis-Lambda-function");
        for (KinesisEvent.KinesisEventRecord r : KinesisEvent.getRecords()) {

            System.out.println(" The Partition Key "+ r.getKinesis().getPartitionKey() );
           // System.out.println(" The decoded data "+ Base64.decode(new String(r.getKinesis().getData().array())));
            System.out.println("The Record Data type :" + r.getKinesis().getData().array().getClass().toString());
            System.out.println("The Record Data : " + new String(r.getKinesis().getData().array()));
            System.out.println("The Record encryption type : " + r.getKinesis().getEncryptionType());
            // System.out.println("after Base64 Decode :"+ new String(Base64.decode(r.getKinesis().getData().array())));
            // System.out.println("after Base64 Decode :"+ new String(Base64.decode(r.getKinesis().getData().array())));
         /*   try {
                String mystr = new String(decompress(new String(String.valueOf(r.getKinesis().getData()))));
                System.out.println("The Record after decompression decoded : " + r.getKinesis().getEncryptionType());
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            data.put(r.getKinesis().getPartitionKey(), new String(r.getKinesis().getData().array()));
           // data.put(r.getKinesis().getPartitionKey(), new String(new GZIPInputStream(new (r.getKinesis().getData().asCharBuffer())));

        }
        try {
            postToendPoint(data, logger, url, http);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static void postToendPoint(Map<String, String> data, LambdaLogger logger, URL url, HttpURLConnection http) throws IOException {
        Set<Map.Entry<String, String>> sets = data.entrySet();


        System.out.println("Data just inside postToEndpoint = :  " + data);

        for (Map.Entry<String, String> records : sets) {

            System.out.println(records.getValue().chars());
            logger.log("Key =" + records.getKey() + "and value =" + records.getValue());
            String rdata = "{\n  \"";
            rdata = rdata + records.getKey();
            rdata = rdata + "\": \"";
            rdata = rdata + records.getValue();
            rdata = rdata + "\" \n }";

            System.out.println("Record data: " + rdata);
            // String string =  "{\"name\": \"Sam Smith\", \"technology\": \"Python\"}";
            byte[] out = rdata.getBytes(StandardCharsets.UTF_8);

            OutputStream stream = http.getOutputStream();
            stream.write(out);

            System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
            http.disconnect();


        }


    }

    private static HttpRequest.BodyPublisher buildFormDataFromMap(Map<String, String> data) {
        var builder = new StringBuilder();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        System.out.println(builder.toString());
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }

    public static String decompress(String str) throws Exception {
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(Base64.decode(str)));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
            outStr += line;
        }
        System.out.println("Decompressed String length : " + outStr.length());
        return outStr;
    }
}
