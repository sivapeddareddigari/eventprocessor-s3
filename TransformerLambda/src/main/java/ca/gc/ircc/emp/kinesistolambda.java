package ca.gc.ircc.emp;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.KinesisEvent;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.http.HttpClient;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.GZIPInputStream;



public class kinesistolambda implements RequestHandler<KinesisEvent, Void> {

    private final HttpClient httpClient = HttpClient.newBuilder().build();


    ObjectMapper om;

    URL url = null;
    String ErrorBucket;


    String loggroup = "LogGroup";
    String logStream = "LogStream";
    String owner = "Owner";
    Map<String, String> payloadMap = new LinkedHashMap<>();
    HashMap<String, String> MessageFieldMap = new LinkedHashMap<>();
    StringBuffer outStr = new StringBuffer();
    HttpURLConnection http = null;


    @Override
    public Void handleRequest(KinesisEvent KinesisEvent, Context context) {

        LambdaLogger logger = context.getLogger();


        ErrorBucket = System.getenv("errorStore");

        Map<String, String> data = new LinkedHashMap<>();

        for (KinesisEvent.KinesisEventRecord r : KinesisEvent.getRecords()) {
            logger.log(" The Partition Key " + r.getKinesis().getPartitionKey());
            ByteArrayInputStream in = new ByteArrayInputStream(r.getKinesis().getData().array());
            try {
                GZIPInputStream gis = new GZIPInputStream(in);

                BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
                String line;
                while ((line = bf.readLine()) != null) {
                    outStr.append(line);
                    outStr.append(System.lineSeparator());
                }
//                logger.log ("Decompressed String Length : " + outStr.length());
                logger.log("Decompressed String  : " + outStr);
            } catch (IOException e) {
                logger.log("inside the IOException block");
                e.printStackTrace();
            }
            logger.log("Length of outStr is :" + outStr.length());

//
            data.put(r.getKinesis().getPartitionKey(), String.valueOf(outStr).trim());
            outStr.setLength(0);

        }
        logger.log("Initial data map :" + data.toString());
        try {
            getPayload(data, logger);
            postToendPoint(payloadMap, logger, http);
        } catch (Exception e) {
            e.printStackTrace();
            logger.log("Message is " + e.getMessage());
        }
        return null;
    }


    private void postToendPoint(Map<String, String> data, LambdaLogger logger, HttpURLConnection http) throws IOException {

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.CA_CENTRAL_1)
                .build();

//        logger.log("the final hashmap contents for POSTing are "+ data.toString());
        Set<Map.Entry<String, String>> sets = data.entrySet();
//        logger.log("HashMap Size is : " + data.size());

        for (Map.Entry<String, String> records : sets) {

            // System.out.println("Record data: " + records.getValue().toString());
            int k=1;
            try {

                url = new URL(System.getenv("fluentURL") + records.getKey().toString());
                logger.log("fluentd endpoint is: " + url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                http = (HttpURLConnection) url.openConnection();

            } catch (IOException e) {
                putLogToS3(ErrorBucket,records.getKey().toString()+ "-"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "-" + k, records.getValue().toString(), s3Client, logger);
                e.printStackTrace();
            }
            try {
                http.setRequestMethod("POST");
            } catch (ProtocolException e) {

                putLogToS3(ErrorBucket,records.getKey().toString()+ "-"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "-" + k, records.getValue().toString(),s3Client, logger);
                e.printStackTrace();

            }
            http.setDoOutput(true);
            http.setRequestProperty("Accept", "application/json");
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("User-Agent", "Kinesis-Lambda-function");

            byte[] out = records.getValue().toString().getBytes(StandardCharsets.UTF_8);
            logger.log("the record value of Map about to be POSTed :");
            logger.log(records.getValue().toString());

            try{
                OutputStream stream = http.getOutputStream();
                stream.write(out);
                logger.log("Response Code & Message: " + http.getResponseCode() + " " + http.getResponseMessage());
            }catch(Exception e){
                logger.log("Error occurred, unable to POST to aggregator; Saving to S3 bucket");
                putLogToS3(ErrorBucket,records.getKey().toString()+ "-"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "-" + k, records.getValue().toString(),s3Client, logger);
            }

            k= k+1;
        }
        http.disconnect();
    }

    private void getPayload(Map<String, String> data1, LambdaLogger logger)  {

        logger.log("getPayload :  hashmap Size is : " + data1.size());
        Iterator it = data1.entrySet().iterator();
        while (it.hasNext()) {
            String deNormStr = "";
            Map.Entry pair = (Map.Entry) it.next();
            String keyVal = pair.getKey().toString().trim();
            logger.log("Key Value Pair set for computing payload :" + pair.getKey() + "; Value :" + pair.getValue().toString().trim());
            getPayloadMap(pair.getKey().toString().trim(), pair.getValue().toString().trim(), "logEvents", logger);

            it.remove(); // avoids a ConcurrentModificationException
        }


    }

    private void getPayloadMap(String key, String str, String ArrayName, LambdaLogger logger)
    {
        //1. read the str and create a json
        Gson gson = new Gson();

        try {

            JSONParser parser = new JSONParser();
            org.json.simple.JSONObject json = (org.json.simple.JSONObject) parser.parse(str);
            owner = json.get("owner").toString().replaceAll("[^a-zA-Z0-9]", "-");
            loggroup = json.get("logGroup").toString().replaceAll("[^a-zA-Z0-9]", "-");
            logStream = json.get("logStream").toString().replaceAll("[^a-zA-Z0-9]", "-");


            // JsonObject dataJSON = new Gson().fromJson(str, JsonObject.class);
            // logger.log("getPayLoadMap function: the string conversion of dataJSON :"+ dataJSON.toString());

            //  owner =(String.valueOf(dataJSON.get("owner")).replaceAll("[^a-zA-Z0-9]", "-"));
            //  loggroup = (String.valueOf(dataJSON.get("logGroup")).replaceAll("[^a-zA-Z0-9]", "-"));
            //   logStream = (String.valueOf(dataJSON.get("logStream")).replaceAll("[^a-zA-Z0-9]", "-"));
            String payLoadKey = owner + "." + loggroup + "." + logStream;

            logger.log("the url key :" + payLoadKey);
            JSONArray logevents = (JSONArray) json.get(ArrayName);
            json.remove(ArrayName);
            //  dataJSON.remove(ArrayName);
            logger.log("JSON after removing the logEvents Array :" + json.toString());
            //JsonElement jsonElement = gson.toJsonTree(dataJSON);


            for (int i = 0; i < logevents.size(); i++) {
           logger.log("The " + i + " element of the array: " + logevents.get(i));

               // JSONObject joOutStr = new JSONObject(logevents.get(i).toString().trim());
                om=new ObjectMapper();
                JsonNode jn = om.readTree(logevents.get(i).toString().trim());


            logger.log("the array after JSON conversion :"+ jn.toString());
                org.json.simple.JSONObject HeaderJson = json;
                HeaderJson.put("PartitionKey", key);
                logger.log("Partition Key :"+ key);
                HeaderJson.put("id", jn.get("id").textValue());
                logger.log(" ID :"+ jn.get("id").textValue());
                HeaderJson.put("timestamp", jn.get("timestamp").textValue());
                logger.log("timestamp :"+ jn.get("timestamp").textValue());
                logger.log("Message field is :"+ jn.get("message").textValue());
                getMessageMap(jn.get("message").textValue() , logger);

                Iterator it = MessageFieldMap.entrySet().iterator();
                while (it.hasNext()) {
                    String deNormStr = "";
                    Map.Entry pair = (Map.Entry) it.next();
                    String keyVal = pair.getKey().toString().trim();
                    logger.log("Key Value of Message :" + pair.getKey() + "; Value :" + pair.getValue().toString().trim());
                    HeaderJson.put(pair.getKey(), pair.getValue().toString().trim());

                    it.remove(); // avoids a ConcurrentModificationException
                }
                //  HeaderJson.put("message", joOutStr.getString("message"));


                payloadMap.put(payLoadKey, HeaderJson.toString());


            }
            logger.log("the payloadMap before POSTing is :" + payloadMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getMessageMap(String msgStr, LambdaLogger logger) throws ParseException, IOException {

        try {

            logger.log("Executing getMessageMap...MsgStr: "+ msgStr);

            JSONParser parser = new JSONParser();





            org.json.simple.JSONObject json = (org.json.simple.JSONObject) parser.parse(msgStr);
            ObjectMapper mapper = new ObjectMapper();

            MessageFieldMap = mapper.readValue(msgStr, HashMap.class);
            /*MessageFieldMap = mapper.readValue(msgStr, new TypeReference<HashMap<String, Object>>() {
                    });*/

        } catch (ParseException pe) {

            logger.log("getMessageMap - Parse Exception : " + pe.getMessage());

        } catch (IOException ie) {
            logger.log("getMessageMap -  IO Exception : " + ie.getMessage());

        }

    }

    public void putLogToS3(String BucketName, String FileName, String FileContent, AmazonS3 Client, LambdaLogger logger) throws IOException {

        try {

            logger.log("Uploading the log file on to S3 Bucket : " + FileName + " ...");
            logger.log("File Content is : " + FileContent);
            logger.log("File Name should be : " + FileName);
            //byte[] out = FileContent.getBytes(StandardCharsets.UTF_8);
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append(FileContent);
            logger.log("..StringBuilder appended..");
            ByteArrayInputStream inputStream = new ByteArrayInputStream(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));

            //InputStream targetStream = new ByteArrayInputStream(out);
            logger.log("..inputStream constructed..");
            ObjectMetadata metadata = new ObjectMetadata();
            byte[] bytes = IOUtils.toByteArray(inputStream);
            Long contentLength = Long.valueOf(bytes.length);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            metadata.setContentLength(contentLength);
            logger.log("ContentLength: " + contentLength);

            PutObjectRequest putRequest = new PutObjectRequest(BucketName, FileName,byteArrayInputStream, metadata);

            String eTag = Client.putObject(putRequest).getETag();


         //   Client.putObject(BucketName, FileName, inputStream, metadata);
            logger.log("... uploaded : "+ eTag);
        } catch (Exception e){
        logger.log("Message : "+e.getMessage());
        e.printStackTrace();
        }
    }
}
